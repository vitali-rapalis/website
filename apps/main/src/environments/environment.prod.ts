import { EnvModel } from '@website/model/model';

export const environment: EnvModel = {
  production: true,
};
