import { Component, HostListener, Inject, OnInit } from '@angular/core';
import { DataService } from '@website/data-access';
import { Observable } from 'rxjs';
import { CommonModel } from '@website/model/model';
import { StyleService } from '@website/utility';
import { DOCUMENT } from '@angular/common';

@Component({
  selector: 'website-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit {
  common$!: Observable<CommonModel>;

  constructor(
    @Inject(DOCUMENT) private document: Document,
    public dService: DataService,
    private sService: StyleService
  ) {}

  ngOnInit(): void {
    this.common$ = this.dService.getCommon();
  }

  @HostListener('window:scroll', ['$event'])
  onScroll() {
    this.sService.addScrollClassOnScroll(
      this.document.getElementsByTagName('body')[0]
    );
  }
}
