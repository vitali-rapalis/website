import { Route } from '@angular/router';

export const appRoutes: Route[] = [
  { path: '', pathMatch: 'full', redirectTo: 'home' },
  {
    path: 'home',
    loadChildren: () =>
      import('@website/feature-home').then((mod) => mod.HomeFeatureModule),
  },
  {
    path: 'cv',
    loadChildren: () =>
      import('@website/feature-cv').then((mod) => mod.CVFeatureModule),
  },
  /*{
    path: 'blog',
    loadChildren: () =>
      import('@website/feature-blog').then((mod) => mod.BlogFeatureModule),
  },
  {
    path: 'contact',
    loadChildren: () =>
      import('@website/feature-contact').then(
        (mod) => mod.ContactFeatureModule
      ),
  },*/
  {
    path: '**',
    redirectTo: 'home',
  },
];
