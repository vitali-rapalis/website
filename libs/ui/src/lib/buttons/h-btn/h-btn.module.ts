import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HBtnComponent } from './components/h-btn.component';

@NgModule({
  declarations: [HBtnComponent],
  imports: [CommonModule],
  exports: [HBtnComponent],
})
export class HBtnModule {}
