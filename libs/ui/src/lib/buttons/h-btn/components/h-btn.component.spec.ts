import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HBtnComponent } from './h-btn.component';
import { HBtnModule } from '../h-btn.module';

describe('HBtnComponent', () => {
  let component: HBtnComponent;
  let fixture: ComponentFixture<HBtnComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HBtnModule],
    }).compileComponents();

    fixture = TestBed.createComponent(HBtnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should after button was clicked set property back and forth false => true => false => true', () => {
    // Given
    expect(component.isMenuClosed).toBeFalsy();

    // When
    const button = getButton(fixture);
    button.click();

    // Then
    expect(component.isMenuClosed).toBeTruthy();

    // When
    button.click();
    expect(component.isMenuClosed).toBeFalsy();
  });

  it('should after button was clicked emit output event', (done) => {
    // Given
    expect(component.isMenuClosed).toBeFalsy();

    // Then
    component.clicked.subscribe(() => {
      expect(component.isMenuClosed).toBeTruthy();
      done();
    });

    // When
    const button = getButton(fixture);
    button.click();
  });
});

function getButton(fixture: ComponentFixture<HBtnComponent>) {
  return fixture.nativeElement.querySelector('button') as HTMLButtonElement;
}
