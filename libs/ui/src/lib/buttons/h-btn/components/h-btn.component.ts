import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'website-h-btn',
  templateUrl: 'h-btn.component.html',
  styleUrl: 'h-btn.component.scss',
})
export class HBtnComponent {
  @Input() isMenuClosed = false;
  @Output() clicked = new EventEmitter<boolean>();

  onHamMenuClick() {
    this.isMenuClosed = !this.isMenuClosed;
    this.clicked.emit(this.isMenuClosed);
  }
}
