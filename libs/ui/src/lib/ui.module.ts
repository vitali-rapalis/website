import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderModule } from './header/header.module';
import { HBtnModule } from './buttons/h-btn/h-btn.module';
import { UiBoxesModule } from './boxes/ui-boxes.module';
import { UiSlidersModule } from './sliders/ui-sliders.module';
import { FooterComponent } from './footer/footer.component';

@NgModule({
  declarations: [FooterComponent],
  imports: [CommonModule],
  exports: [
    HeaderModule,
    HBtnModule,
    UiBoxesModule,
    UiSlidersModule,
    FooterComponent,
  ],
})
export class UiModule {}
