import { NgModule } from '@angular/core';
import { HeaderContainerComponent } from './components/header-container.component';
import { HLogoComponent } from './components/h-logo/h-logo.component';
import { HMenuComponent } from './components/h-menu/h-menu.component';
import { HBtnModule } from '../buttons/h-btn/h-btn.module';
import { RouterLink, RouterLinkActive } from '@angular/router';
import { CommonModule, NgOptimizedImage } from '@angular/common';

@NgModule({
  declarations: [HeaderContainerComponent, HLogoComponent, HMenuComponent],
  imports: [
    CommonModule,
    HBtnModule,
    RouterLink,
    RouterLinkActive,
    NgOptimizedImage,
  ],
  exports: [HeaderContainerComponent],
})
export class HeaderModule {}
