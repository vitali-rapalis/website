import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HMenuComponent } from './h-menu.component';
import { HeaderModule } from '../../header.module';
import { RouterTestingModule } from '@angular/router/testing';
import { NgOptimizedImage } from '@angular/common';

describe('HMenuComponent', () => {
  let component: HMenuComponent;
  let fixture: ComponentFixture<HMenuComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HeaderModule, RouterTestingModule, NgOptimizedImage],
    }).compileComponents();

    fixture = TestBed.createComponent(HMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should open mobile menu', () => {
    // When
    expect(fixture.componentInstance.isMenuClosed).toBeFalsy();
    const menuButton = fixture.nativeElement
      .getElementsByClassName('ham-menu-btn')
      ?.item(0);
    menuButton.click();
    fixture.detectChanges();
    const hasBodyOpenClass = document.body.classList.contains('open');

    // Then
    expect(fixture.componentInstance.isMenuClosed).toBeTruthy();
    expect(hasBodyOpenClass).toBeTruthy();
  });
});
