import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
  PLATFORM_ID,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';
import { BreakpointObserver } from '@angular/cdk/layout';
import { LanguageService } from '@website/utility';
import { Subject, takeUntil } from 'rxjs';
import { NavModel } from '@website/model/model';

@Component({
  selector: 'website-h-menu',
  styleUrl: 'h-menu.component.scss',
  templateUrl: 'h-menu.component.html',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HMenuComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChild('navigation')
  private navigationRef?: ElementRef<HTMLElement>;
  @ViewChild('enLang')
  private enLangImg?: ElementRef<HTMLElement>;
  @ViewChild('deLang')
  private deLangImg?: ElementRef<HTMLElement>;
  @ViewChild('ruLang')
  private ruLangImg?: ElementRef<HTMLElement>;
  isMenuClosed = false;
  private unsubscribe$ = new Subject<void>();
  @Output() switchLanguageEvent = new EventEmitter<string>();
  @Output() navigateEvent = new EventEmitter<string>();
  @Input()
  nav!: NavModel;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    @Inject(DOCUMENT) private document: Document,
    private bService: BreakpointObserver,
    private lService: LanguageService
  ) {}

  ngOnInit(): void {
    this.lService
      .onLanguageSelect()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((lang) => this.onLanguageSwitch(lang));
  }

  private onLanguageSwitch(lang: string) {
    this.enLangImg?.nativeElement.classList.remove('en');
    this.deLangImg?.nativeElement.classList.remove('de');
    this.ruLangImg?.nativeElement.classList.remove('ru');

    if (lang === 'en') {
      this.enLangImg?.nativeElement.classList.add(lang);
      return;
    }

    if (lang === 'de') {
      this.deLangImg?.nativeElement.classList.add(lang);
      return;
    }

    if (lang === 'ru') {
      this.ruLangImg?.nativeElement.classList.add(lang);
      return;
    }
  }

  ngAfterViewInit(): void {
    this.onLanguageSwitch(this.lService.currentLanguage);
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onMenuClick() {
    if (
      isPlatformBrowser(this.platformId) &&
      !this.bService.isMatched('(min-width:1200px)')
    ) {
      this.isMenuClosed = !this.isMenuClosed;
      this.navigationRef?.nativeElement.classList.toggle('open');
      this.document
        .getElementsByTagName('main')
        ?.item(0)
        ?.classList.toggle('blur');
      this.document
        .getElementsByTagName('body')
        ?.item(0)
        ?.classList.toggle('open');
    }
  }

  switchLanguage(lang: string) {
    this.switchLanguageEvent.next(lang);
  }

  navigateTo(route: string) {
    this.navigateEvent.next(route);
  }
}
