import {
  Component,
  ElementRef,
  HostListener,
  Inject,
  Input,
  OnDestroy,
  PLATFORM_ID,
  ViewChild,
  ViewEncapsulation,
} from '@angular/core';
import { LanguageService, StyleService } from '@website/utility';
import { Subject, takeUntil } from 'rxjs';
import { Router } from '@angular/router';
import { NavModel } from '@website/model/model';

@Component({
  selector: 'website-header',
  template: `
    <header #header>
      <div class="h-line"></div>
      <div class="row box align-items-center justify-content-between">
        <website-h-logo
          [short]="labelShort"
          [long]="labelLong"
          class="col-8 col-xl-4"
        ></website-h-logo>
        <website-h-menu
          (switchLanguageEvent)="onLanguageSwitch($event)"
          (navigateEvent)="onNavigate($event)"
          [nav]="nav"
          class="col-auto col-xl-8"
        ></website-h-menu>
      </div>
    </header>
  `,
  styleUrl: 'header-container.component.scss',
  encapsulation: ViewEncapsulation.None,
})
export class HeaderContainerComponent implements OnDestroy {
  @ViewChild('header')
  private header?: ElementRef<HTMLHeadingElement>;
  private readonly unsubscribe$ = new Subject<void>();
  @Input()
  labelLong?: string;
  @Input()
  labelShort?: string;
  @Input()
  nav!: NavModel;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    private lService: LanguageService,
    private router: Router,
    private sService: StyleService
  ) {}

  @HostListener('window:scroll', ['$event'])
  onScroll() {
    this.sService.addScrollClassOnScroll(this.header?.nativeElement);
  }

  onLanguageSwitch(lang: string) {
    this.lService
      .switchLanguage(lang)
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe();
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onNavigate(route: string) {
    this.router.navigate([route], {
      queryParams: { lang: this.lService.currentLanguage },
    });
  }
}
