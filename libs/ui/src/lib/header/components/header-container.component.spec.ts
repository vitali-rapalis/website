import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderContainerComponent } from './header-container.component';
import { HeaderModule } from '../header.module';
import { RouterTestingModule } from '@angular/router/testing';
import spyOn = jest.spyOn;

describe('HeaderContainerComponent', () => {
  let component: HeaderContainerComponent;
  let fixture: ComponentFixture<HeaderContainerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HeaderModule, RouterTestingModule],
    }).compileComponents();

    fixture = TestBed.createComponent(HeaderContainerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should add scroll class to header if scrollTop is higher', () => {
    // When
    spyOn(document.body, 'scrollTop', 'get').mockReturnValue(50);
    window.dispatchEvent(new Event('scroll'));
    fixture.detectChanges();
    const header = fixture.nativeElement
      .getElementsByTagName('header')
      ?.item(0);

    // Then
    expect(header.classList.contains('scroll')).toBeTruthy();
  });

  it('should remove scroll class from header if scrollTop is lower', () => {
    // When
    spyOn(document.body, 'scrollTop', 'get').mockReturnValue(10);
    window.dispatchEvent(new Event('scroll'));
    fixture.detectChanges();
    const header = fixture.nativeElement
      .getElementsByTagName('header')
      ?.item(0);

    // Then
    expect(header.classList.contains('scroll')).toBeFalsy();
  });
});
