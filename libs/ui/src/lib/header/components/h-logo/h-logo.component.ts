import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'website-h-logo',
  changeDetection: ChangeDetectionStrategy.OnPush,
  template: `
    <label routerLink="home">
      <p class="m-0">{{ long }}</p>
    </label>
  `,
  styles: [
    `
      label {
        & p {
          text-transform: uppercase;
          font-weight: bold;
          font-family: 'Ubuntu Bold' !important;
          font-size: 0.9em;
          color: black;

          span {
            font-size: 1em;
            font-family: 'Ubuntu Bold' !important;
          }
        }
      }
    `,
  ],
})
export class HLogoComponent {
  @Input() short: string | undefined;
  @Input() long: string | undefined;
}
