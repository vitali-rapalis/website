import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HLogoComponent } from './h-logo.component';
import { HeaderModule } from '../../header.module';
import { RouterTestingModule } from '@angular/router/testing';
import { NgOptimizedImage } from '@angular/common';

describe('LogoComponent', () => {
  let component: HLogoComponent;
  let fixture: ComponentFixture<HLogoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HeaderModule, RouterTestingModule, NgOptimizedImage],
    }).compileComponents();

    fixture = TestBed.createComponent(HLogoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
