import { Component, OnDestroy, ViewEncapsulation } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'website-project-slider',
  templateUrl: './project-slider.component.html',
  styleUrl: './project-slider.component.scss',
  encapsulation: ViewEncapsulation.None,
})
export class ProjectSliderComponent implements OnDestroy {
  images: Array<object> = [
    {
      thumbImage: 'assets/images/project-slider/dzb-icon.svg',
      alt: 'Dzb',
      order: 1,
    },
    {
      thumbImage: 'assets/images/project-slider/schaeffler-icon.svg',
      alt: 'Schaeffler',
    },
    {
      thumbImage: 'assets/images/project-slider/msg-icon.svg',
      alt: 'Msg',
    },
    {
      thumbImage: 'assets/images/project-slider/schwarz-icon.svg',
      alt: 'Schwarz It',
    },
    {
      thumbImage: 'assets/images/project-slider/lidl-icon.svg',
      alt: 'Lidl',
    },
    {
      thumbImage: 'assets/images/project-slider/smawandi-icon.svg',
      alt: 'Smawandi',
    },
  ];
  private unsubscribe$ = new Subject<void>();
  imageWidth = 200;
  imageHeight = 70;
  imageSpace = 30;

  constructor(breakpointObserver: BreakpointObserver) {
    breakpointObserver
      .observe('(max-width: 768px)')
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((matcher) => this.changeImageSize(matcher.matches));
  }

  changeImageSize = (isMaxMediumScreen: boolean) => {
    if (isMaxMediumScreen) {
      this.imageWidth = 120;
      this.imageHeight = 100;
      this.imageSpace = 10;
    } else {
      this.imageWidth = 180;
      this.imageHeight = 120;
      this.imageSpace = 5;
    }
  };

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  onImageClick($event: number) {
    console.log($event);
  }
}
