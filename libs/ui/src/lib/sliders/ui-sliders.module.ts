import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProjectSliderComponent } from './project-slider/project-slider.component';
import { NgImageSliderModule } from 'ng-image-slider-v17';

@NgModule({
  declarations: [ProjectSliderComponent],
  imports: [CommonModule, NgImageSliderModule],
  exports: [ProjectSliderComponent],
})
export class UiSlidersModule {}
