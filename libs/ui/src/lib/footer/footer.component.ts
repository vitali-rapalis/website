import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { LanguageService } from '@website/utility';
import { Router } from '@angular/router';
import { NavModel } from '@website/model/model';

@Component({
  selector: 'website-footer',
  templateUrl: './footer.component.html',
  styleUrl: './footer.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooterComponent {
  @Input()
  nav!: NavModel;

  constructor(private lService: LanguageService, private router: Router) {}

  navigateTo(route: string) {
    this.router.navigate([route], {
      queryParams: { lang: this.lService.currentLanguage },
    });
  }
}
