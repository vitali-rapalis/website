import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageBoxComponent } from './image-box.component';
import { UiBoxesModule } from '../ui-boxes.module';
import { MarkdownModule } from 'ngx-markdown';

describe('ImageBoxComponent', () => {
  let component: ImageBoxComponent;
  let fixture: ComponentFixture<ImageBoxComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [UiBoxesModule, MarkdownModule.forRoot()],
    }).compileComponents();

    fixture = TestBed.createComponent(ImageBoxComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should set title', () => {
    // Given
    const title = 'Title';

    // When
    fixture.componentRef.setInput('title', title);
    fixture.detectChanges();

    // Then
    const fountTitleEl = fixture.nativeElement.querySelector('.title');
    expect(fountTitleEl.textContent).toEqual(title);
  });

  it('should set subtitle', () => {
    const fountSubtitleEl = fixture.nativeElement.querySelector('.subtitle');
    expect(fountSubtitleEl.textContent).toBeTruthy();
  });

  it('should set link', () => {
    const fountSubtitleEl = fixture.nativeElement.querySelector('.link');
    expect(fountSubtitleEl.textContent).toBeTruthy();
  });
});
