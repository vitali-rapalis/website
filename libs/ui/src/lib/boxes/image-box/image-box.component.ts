import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'website-image-box',
  templateUrl: './image-box.component.html',
  styleUrl: './image-box.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageBoxComponent {
  @Input() title = 'In search of a senior full-stack software developer?';
  @Input() subtitle =
    'I am using <strong>Spring Boot</strong> and <strong>Angular</strong>!';
  @Input() imgSrc = 'assets/images/home/person.png';
  @Input() btnTxt = 'More';
}
