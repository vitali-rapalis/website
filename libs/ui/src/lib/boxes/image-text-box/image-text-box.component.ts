import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { ImageBoxModel } from '@website/model/model';

@Component({
  selector: 'website-image-text-box',
  templateUrl: './image-text-box.component.html',
  styleUrl: './image-text-box.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageTextBoxComponent {
  @Input() boxes?: Array<ImageBoxModel>;
}
