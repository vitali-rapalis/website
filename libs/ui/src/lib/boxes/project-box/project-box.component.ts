import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { ProjectBoxModel } from '@website/model/model';

@Component({
  selector: 'website-project-box',
  templateUrl: './project-box.component.html',
  styleUrl: './project-box.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectBoxComponent {
  @Input() title?: string;
  @Input() projects?: Array<ProjectBoxModel>;
}
