import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { ContactBoxModel } from '@website/model/model';

@Component({
  selector: 'website-contact-box',
  templateUrl: './contact-box.component.html',
  styleUrl: './contact-box.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContactBoxComponent {
  @Input() contact?: ContactBoxModel;
}
