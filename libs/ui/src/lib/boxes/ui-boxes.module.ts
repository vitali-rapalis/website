import { NgModule } from '@angular/core';
import { CommonModule, NgOptimizedImage } from '@angular/common';
import { ImageBoxComponent } from './image-box/image-box.component';
import { InfoBoxComponent } from './info-box/info-box.component';
import { ImageTextBoxComponent } from './image-text-box/image-text-box.component';
import { ContactBoxComponent } from './contact-box/contact-box.component';
import { MarkdownComponent, MarkdownModule } from 'ngx-markdown';
import { ProjectBoxComponent } from './project-box/project-box.component';

@NgModule({
  imports: [
    CommonModule,
    NgOptimizedImage,
    MarkdownComponent,
    MarkdownModule.forChild(),
  ],
  declarations: [
    ImageBoxComponent,
    InfoBoxComponent,
    ImageTextBoxComponent,
    ContactBoxComponent,
    ProjectBoxComponent,
  ],
  exports: [
    ImageBoxComponent,
    InfoBoxComponent,
    ImageTextBoxComponent,
    ContactBoxComponent,
    ProjectBoxComponent,
  ],
})
export class UiBoxesModule {}
