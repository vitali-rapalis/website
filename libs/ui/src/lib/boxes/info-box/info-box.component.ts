import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';
import { InfoBoxModel } from '@website/model/model';

@Component({
  selector: 'website-info-box',
  templateUrl: './info-box.component.html',
  styleUrl: './info-box.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class InfoBoxComponent {
  @Input() box?: InfoBoxModel;
}
