import {
  Inject,
  Injectable,
  PLATFORM_ID,
  Renderer2,
  RendererFactory2,
} from '@angular/core';
import { DOCUMENT, isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class StyleService {
  private renderer: Renderer2;

  constructor(
    @Inject(PLATFORM_ID) private platformId: object,
    @Inject(DOCUMENT) private document: Document,
    rendererFactory: RendererFactory2
  ) {
    this.renderer = rendererFactory.createRenderer(null, null);
  }

  addScrollClassOnScroll(el: HTMLElement | undefined) {
    if (el !== undefined && isPlatformBrowser(this.platformId)) {
      if (
        this.document.body.scrollTop > 25 ||
        this.document.documentElement.scrollTop > 25
      ) {
        this.renderer.addClass(el, 'scroll');
      } else {
        this.renderer.removeClass(el, 'scroll');
      }
    }
  }
}
