import { InjectionToken } from '@angular/core';
import { EnvModel } from '@website/model/model';

export const ENV_TOKEN = new InjectionToken<EnvModel>('Environment token');
