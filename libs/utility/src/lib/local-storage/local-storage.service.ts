import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { Observable, of } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';

@Injectable({
  providedIn: 'root',
})
export class LocalStorageService {
  private isBrowser = false;

  constructor(@Inject(PLATFORM_ID) private platformId: string) {
    this.isBrowser = isPlatformBrowser(platformId);
  }

  getKey(key: string): Observable<string | null> {
    return this.isBrowser ? of(localStorage.getItem(key)) : of('');
  }

  setItem(key: string, keyValue: string): Observable<void> {
    return this.isBrowser ? of(localStorage.setItem(key, keyValue)) : of();
  }

  setItemAsString(givenKey: string, givenKeyValue: string) {
    if (this.isBrowser) {
      localStorage.setItem(givenKey, givenKeyValue);
    }
  }

  getKeyAsString(givenKey: string): string | null {
    if (!this.isBrowser) {
      return null;
    }

    return localStorage.getItem(givenKey);
  }
}
