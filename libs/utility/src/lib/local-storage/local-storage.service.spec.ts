import { TestBed } from '@angular/core/testing';

import { LocalStorageService } from './local-storage.service';
import { switchMap } from 'rxjs';

describe('LStorageService', () => {
  let localStorageService: LocalStorageService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    localStorageService = TestBed.inject(LocalStorageService);
  });

  it('should be created', () => {
    expect(localStorageService).toBeTruthy();
  });

  it('should read not existing key from storage', (done) => {
    // When
    const keyValue = localStorageService.getKey('key-name');

    // Then
    keyValue.subscribe((value) => {
      expect(value).toBeNull();
      done();
    });
  });

  it('should read existing key from storage', (done) => {
    // Given
    const givenKey = 'key';
    const givenKeyValue = 'key-value';

    // When
    const setKeyObservable = localStorageService.setItem(
      givenKey,
      givenKeyValue
    );

    // Then
    setKeyObservable
      .pipe(switchMap(() => localStorageService.getKey(givenKey)))
      .subscribe((keyValue) => {
        expect(keyValue).toEqual(givenKeyValue);
        done();
      });
  });

  it('should read existing key and returning string', () => {
    // Given
    const givenKey = 'key';
    const givenKeyValue = 'key-value';

    // When
    localStorageService.setItemAsString(givenKey, givenKeyValue);
    const keyValue = localStorageService.getKeyAsString(givenKey);

    // Then
    expect(keyValue).toEqual(givenKeyValue);
  });
});
