import { TestBed } from '@angular/core/testing';

import { LanguageService } from './language.service';
import { switchMap } from 'rxjs';
import { LocalStorageService } from '../local-storage/local-storage.service';
import { RouterTestingModule } from '@angular/router/testing';

describe('LanguageService', () => {
  let service: LanguageService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [RouterTestingModule],
      providers: [LocalStorageService],
    });

    service = TestBed.inject(LanguageService);
  });

  it('should be created', () => {
    // Then
    expect(service).toBeTruthy();
  });

  it('should provide observable stream of selected language', (done) => {
    // When
    service.onLanguageSelect().subscribe((language) => {
      // Then
      expect(language).toEqual(LanguageService.INIT_LANGUAGE);
      done();
    });
  });

  it('should switch the language', (done) => {
    // Given
    const givenLanguage = 'rus';

    // When
    service
      .switchLanguage(givenLanguage)
      .pipe(switchMap(service.onLanguageSelect))
      .subscribe((language) => {
        // Then
        expect(language).toEqual(givenLanguage);
        expect(service.currentLanguage).toEqual(givenLanguage);
        done();
      });
  });

  it('should not call local storage service and refresh key if language is not changed', (done) => {
    // Given
    const localStorageService = TestBed.inject(LocalStorageService);
    const spyLocalStorageService = jest.spyOn(
      localStorageService,
      'setItemAsString'
    );

    // When
    service.switchLanguage(LanguageService.INIT_LANGUAGE).subscribe(() => {
      // Then
      expect(spyLocalStorageService).not.toHaveBeenCalled();
      done();
    });
  });
});
