import { Inject, Injectable, PLATFORM_ID } from '@angular/core';
import { BehaviorSubject, map, of, switchMap } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';
import { LocalStorageService } from '../local-storage/local-storage.service';
import { ActivatedRoute, Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class LanguageService {
  public static readonly INIT_LANGUAGE = 'en';
  private readonly supportedLanguages = [
    LanguageService.INIT_LANGUAGE,
    'de',
    'ru',
  ];
  private readonly language = new BehaviorSubject<string>(
    LanguageService.INIT_LANGUAGE
  );
  private isBrowser = false;
  private readonly LANGUAGE_KEY_LOCAL_STORE = 'vitali-rapalis.website';
  public currentLanguage = this.language.getValue();

  constructor(
    @Inject(PLATFORM_ID) platformId: string,
    private lStorage: LocalStorageService,
    private router: Router,
    private aRoute: ActivatedRoute
  ) {
    this.init(platformId);
  }

  onLanguageSelect = () => this.language.asObservable();

  switchLanguage = (language = LanguageService.INIT_LANGUAGE) => {
    if (this.currentLanguage === language) {
      return of<void>(undefined);
    }

    this.currentLanguage = language;
    this.lStorage.setItemAsString(
      this.LANGUAGE_KEY_LOCAL_STORE,
      this.currentLanguage
    );
    return of(this.language.next(language));
  };

  private init(platformId: string) {
    this.isBrowser = isPlatformBrowser(platformId);

    this.aRoute.queryParams
      .pipe(
        map((params) => params['lang']),
        switchMap((pLang) =>
          this.switchLanguage(pLang ? pLang : this.getDefaultBrowserLanguage())
        )
      )
      .subscribe();

    this.onLanguageSelect().subscribe((changedLang) => {
      if (this.isBrowser) {
        const pathname = window.location.pathname.replace('/website', '');
        this.router.navigate([pathname], {
          queryParams: { lang: changedLang },
        });
      }
    });
  }

  private getDefaultBrowserLanguage() {
    if (!this.isBrowser) {
      return LanguageService.INIT_LANGUAGE;
    }

    return this.getLanguageFromLocalStore()
      ? this.getLanguageFromLocalStore()
      : this.getSupportedLanguage();
  }

  /**
   * <strong>Important!</strong> Not ready for server, should be executed in if (isBrowser) statement.
   * <br>
   * <br>
   * Retrieve the supported language based on the user's browser language.
   *
   * @private
   * @returns {string} The supported language.
   */
  private getSupportedLanguage(): string {
    const supportedLanguage = this.supportedLanguages.includes(
      navigator.language
    )
      ? navigator.language
      : LanguageService.INIT_LANGUAGE;
    this.lStorage.setItemAsString(
      this.LANGUAGE_KEY_LOCAL_STORE,
      supportedLanguage
    );
    return supportedLanguage;
  }

  /**
   * <strong>Important!</strong> Not ready for server, should be executed in if (isBrowser) statement.
   * @private
   * @returns {string} The language from the local storage.
   */
  private getLanguageFromLocalStore(): string {
    const language =
      this.lStorage.getKeyAsString(this.LANGUAGE_KEY_LOCAL_STORE) || '';
    return this.supportedLanguages.includes(language) ? language : '';
  }
}
