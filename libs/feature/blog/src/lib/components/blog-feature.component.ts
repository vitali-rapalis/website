import { Component } from '@angular/core';

@Component({
  selector: 'website-feature-blog',
  templateUrl: './blog-feature.component.html',
  styleUrls: ['./blog-feature.component.scss'],
})
export class BlogFeatureComponent {}
