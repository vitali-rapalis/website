import { ComponentFixture, TestBed } from '@angular/core/testing';
import { BlogFeatureComponent } from './blog-feature.component';
import { BlogFeatureModule } from '../blog-feature.module';

describe('FeatureBlogComponent', () => {
  let component: BlogFeatureComponent;
  let fixture: ComponentFixture<BlogFeatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [BlogFeatureModule],
    }).compileComponents();

    fixture = TestBed.createComponent(BlogFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
