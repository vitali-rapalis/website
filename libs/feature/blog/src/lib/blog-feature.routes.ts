import { Route } from '@angular/router';
import { BlogFeatureComponent } from './components/blog-feature.component';

export const blogFeatureRoutes: Route[] = [
  { path: '', component: BlogFeatureComponent },
];
