import { NgModule } from '@angular/core';
import { BlogFeatureComponent } from './components/blog-feature.component';
import { RouterModule } from '@angular/router';
import { blogFeatureRoutes } from './blog-feature.routes';

@NgModule({
  declarations: [BlogFeatureComponent],
  imports: [RouterModule.forChild(blogFeatureRoutes)],
})
export class BlogFeatureModule {}
