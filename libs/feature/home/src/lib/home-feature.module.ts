import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { homeFeatureRoutes } from './home-feature.routes';
import { HomeContainerComponent } from './components/home-container.component';
import { UiBoxesModule, UiSlidersModule } from '@website/ui';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { CommonModule } from '@angular/common';
import { MarkdownModule } from 'ngx-markdown';

@NgModule({
  declarations: [HomeContainerComponent, AboutMeComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(homeFeatureRoutes),
    UiBoxesModule,
    UiSlidersModule,
    MarkdownModule.forChild(),
  ],
  providers: [],
})
export class HomeFeatureModule {}
