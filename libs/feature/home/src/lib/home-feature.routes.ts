import { Route } from '@angular/router';
import { HomeContainerComponent } from './components/home-container.component';

export const homeFeatureRoutes: Route[] = [
  { path: '', pathMatch: 'full', component: HomeContainerComponent },
];
