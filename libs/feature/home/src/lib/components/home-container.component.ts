import { Component } from '@angular/core';
import { DataService } from '@website/data-access';
import { Observable } from 'rxjs';
import { HomeModel } from '@website/model/model';

@Component({
  selector: 'website-home-container',
  template: `
    <div *ngIf="home$ | async as home">
      <website-image-box
        [title]="home.title"
        [subtitle]="home.subtitle"
        [btnTxt]="home.btnTxt"
      ></website-image-box>
      <website-project-slider></website-project-slider>
      <website-about-me [aboutMe]="home.aboutMe"></website-about-me>
      <website-image-text-box [boxes]="home.services"></website-image-text-box>
      <website-info-box [box]="home.info"></website-info-box>
      <website-project-box
        [title]="home.projects.title"
        [projects]="home.projects.list"
      ></website-project-box>
      <website-contact-box [contact]="home.contact"></website-contact-box>
    </div>
  `,
  styles: ``,
})
export class HomeContainerComponent {
  home$: Observable<HomeModel>;

  constructor(private dataService: DataService) {
    this.home$ = dataService.getHome();
  }
}
