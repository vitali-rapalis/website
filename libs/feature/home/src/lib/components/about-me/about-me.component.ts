import {
  ChangeDetectionStrategy,
  Component,
  Input,
  ViewEncapsulation,
} from '@angular/core';

@Component({
  selector: 'website-about-me',
  templateUrl: './about-me.component.html',
  styleUrl: './about-me.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AboutMeComponent {
  @Input() aboutMe!: string;
}
