import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnDestroy,
  PLATFORM_ID,
  ViewEncapsulation,
} from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Subject, takeUntil } from 'rxjs';
import { isPlatformBrowser } from '@angular/common';

@Component({
  selector: 'website-cv-feature-pdf',
  templateUrl: './cv-feature-pdf.component.html',
  styleUrl: './cv-feature-pdf.component.scss',
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CvFeaturePdfComponent implements OnDestroy {
  @Input() src? = 'assets/cv/Vitali_Rapalis_CV_EN.pdf';
  @Input() downloadedPdfFileName?: string;
  private readonly mobileHeight = '600px';
  private readonly webHeight = '2200px';
  private readonly unsubscribe$ = new Subject<void>();
  height = this.mobileHeight;

  constructor(
    @Inject(PLATFORM_ID) platformId: string,
    private breakpointService: BreakpointObserver
  ) {
    if (isPlatformBrowser(platformId)) {
      this.breakpointService
        .observe([Breakpoints.Web])
        .pipe(takeUntil(this.unsubscribe$))
        .subscribe((breakpoint) =>
          breakpoint.matches
            ? (this.height = this.webHeight)
            : (this.height = this.mobileHeight)
        );
    }
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }
}
