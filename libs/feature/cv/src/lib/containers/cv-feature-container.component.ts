import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { LanguageService } from '@website/utility';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'website-cv-feature-container',
  templateUrl: './cv-feature-container.component.html',
  styleUrl: './cv-feature-container.component.scss',
  encapsulation: ViewEncapsulation.None,
})
export class CvFeatureContainerComponent implements OnInit, OnDestroy {
  private unsubscribe$ = new Subject<void>();
  src = 'assets/cv';
  pdfSrc?: string;
  downloadedPdfFileName?: string;
  pdfEnFileName = 'vitali_rapalis_cv_en.pdf';
  pdfDeFileName = 'vitali_rapalis_cv_de.pdf';
  pdfRuFileName = 'виталий_рапалис_резюме.pdf';

  constructor(private lService: LanguageService) {}

  ngOnInit(): void {
    this.lService
      .onLanguageSelect()
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe((lang) => this.setPdfSrc(lang));
  }

  ngOnDestroy(): void {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  private setPdfSrc(lang: string) {
    if (lang.toLowerCase() === 'en') {
      this.pdfSrc = `${this.src}/${this.pdfEnFileName}`;
      this.downloadedPdfFileName = this.pdfEnFileName;
      return;
    }

    if (lang.toLowerCase() === 'de') {
      this.pdfSrc = `${this.src}/${this.pdfDeFileName}`;
      this.downloadedPdfFileName = this.pdfDeFileName;
      return;
    }
    if (lang.toLowerCase() === 'ru') {
      this.pdfSrc = `${this.src}/${this.pdfRuFileName}`;
      this.downloadedPdfFileName = this.pdfRuFileName;
      return;
    }

    this.pdfSrc = `${this.src}/${this.pdfRuFileName}`;
    this.downloadedPdfFileName = this.pdfEnFileName;
  }
}
