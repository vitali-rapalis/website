import { Route } from '@angular/router';
import { CvFeatureContainerComponent } from './containers/cv-feature-container.component';

export const cvFeatureRoutes: Route[] = [
  { path: '', pathMatch: 'full', component: CvFeatureContainerComponent },
];
