import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { cvFeatureRoutes } from './cv-feature.routes';
import { CvFeatureContainerComponent } from './containers/cv-feature-container.component';
import { CvFeaturePdfComponent } from './components/cv-feature-pdf.component';
import { CommonModule } from '@angular/common';
import { MarkdownModule } from 'ngx-markdown';
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';

@NgModule({
  declarations: [CvFeatureContainerComponent, CvFeaturePdfComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(cvFeatureRoutes),
    MarkdownModule.forChild(),
    NgxExtendedPdfViewerModule,
  ],
})
export class CVFeatureModule {}
