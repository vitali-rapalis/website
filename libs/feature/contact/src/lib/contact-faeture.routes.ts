import { Route } from '@angular/router';
import { ContactFeatureComponent } from './components/contact-feature.component';

export const contactFeatureRoutes: Route[] = [
  { path: '', component: ContactFeatureComponent },
];
