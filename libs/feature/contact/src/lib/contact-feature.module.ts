import { NgModule } from '@angular/core';
import { ContactFeatureComponent } from './components/contact-feature.component';
import { RouterModule } from '@angular/router';
import { contactFeatureRoutes } from './contact-faeture.routes';

@NgModule({
  declarations: [ContactFeatureComponent],
  imports: [RouterModule.forChild(contactFeatureRoutes)],
})
export class ContactFeatureModule {}
