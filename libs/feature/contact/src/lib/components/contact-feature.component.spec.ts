import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContactFeatureComponent } from './contact-feature.component';
import { ContactFeatureModule } from '../contact-feature.module';

describe('FeatureContactComponent', () => {
  let component: ContactFeatureComponent;
  let fixture: ComponentFixture<ContactFeatureComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [ContactFeatureModule],
    }).compileComponents();

    fixture = TestBed.createComponent(ContactFeatureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
