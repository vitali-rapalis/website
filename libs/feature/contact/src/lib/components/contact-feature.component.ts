import { Component } from '@angular/core';

@Component({
  selector: 'website-feature-contact',
  templateUrl: './contact-feature.component.html',
  styleUrls: ['./contact-feature.component.scss'],
})
export class ContactFeatureComponent {}
