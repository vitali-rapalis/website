import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ENV_TOKEN, LanguageService } from '@website/utility';
import { mergeMap, Observable } from 'rxjs';
import { CommonModel, EnvModel, HomeModel } from '@website/model/model';

@Injectable({
  providedIn: 'root',
})
export class DataService {
  private baseUrl = '';
  private readonly serverBaseUrl = 'https://vitali-rapalis.gitlab.io/website';

  constructor(
    private http: HttpClient,
    private lService: LanguageService,
    @Inject(ENV_TOKEN) private env: EnvModel
  ) {
    if (env.production) {
      this.baseUrl = this.serverBaseUrl;
    } else {
      this.baseUrl = '';
    }
  }

  getCommon = (): Observable<CommonModel> =>
    this.lService.onLanguageSelect().pipe(
      mergeMap((lang) =>
        this.http.get<CommonModel>(
          `${this.baseUrl}/assets/data/common.${lang}.json`,
          {
            observe: 'body',
          }
        )
      )
    );

  getHome = (): Observable<HomeModel> =>
    this.lService.onLanguageSelect().pipe(
      mergeMap((lang) =>
        this.http.get<HomeModel>(
          `${this.baseUrl}/assets/data/home/home.${lang}.json`,
          {
            observe: 'body',
          }
        )
      )
    );
}
