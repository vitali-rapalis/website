export interface InfoBoxModel {
  readonly title: string;
  readonly text: string;
}

export type ContactBoxModel = InfoBoxModel;

export interface ImageBoxModel {
  readonly imgSrc: string;
  readonly title: string;
  readonly text: string;
}

export interface ProjectBoxModel extends ImageBoxModel {
  readonly stack: string;
  readonly link: string;
  readonly subtitle: string;
}
