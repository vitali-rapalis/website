export interface NavModel {
  readonly home: string;
  readonly cv: string;
  readonly blog: string;
  readonly contact: string;
}

export interface CommonModel {
  readonly header: {
    readonly label: {
      readonly long: string;
      readonly short: string;
    };
    readonly nav: NavModel;
  };
}
