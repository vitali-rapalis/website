import {
  ContactBoxModel,
  ImageBoxModel,
  InfoBoxModel,
  ProjectBoxModel,
} from './ui.model';

export interface HomeModel {
  readonly title: string;
  readonly subtitle: string;
  readonly btnTxt: string;
  readonly aboutMe: string;
  readonly services: Array<ImageBoxModel>;
  readonly info: InfoBoxModel;
  readonly projects: {
    readonly title: string;
    readonly list: Array<ProjectBoxModel>;
  };
  readonly contact: ContactBoxModel;
}
